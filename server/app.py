from flask import Flask, jsonify, request
from flask_cors import CORS


# Configuration
DEBUG = True

# Instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# Enable CORS
CORS(app)


materias = [
    {
        "id": 1,
        "cod": "001",
        "title": "Verificación y Validación",
        "teacher": "Hayser",
    },
    {
        "id": 2,
        "cod": "002",
        "title": "Base de Datos 1",
        "teacher": "Marco Brito",
    },
    {
        "id": 3,
        "cod": "003",
        "title": "Base de Datos 2",
        "teacher": "Marco Brito",
    }
]

horarios = [
    {
        "id": 1,
        "materia_cod": "001",
        "day": "Viernes",
        "horario": "8pm-10pm",
        "title": "Verificación y Validación",
        "is_selected": False
    },
    {
        "id": 2,
        "materia_cod": "001",
        "day": "Viernes",
        "horario": "12pm-2pm",
        "title": "Verificación y Validación",
        "is_selected": False
    },
    {
        "id": 3,
        "materia_cod": "003",
        "day": "Lunes",
        "horario": "7pm-10pm",
        "title": "Base de Datos 2",
        "is_selected": False
    },
    {
        "id": 4,
        "materia_cod": "002",
        "day": "Martes",
        "horario": "8pm-10pm",
        "title": "Base de Datos 1",
        "is_selected": False
    }
]


# Routes
@app.route('/materias', methods=['GET'])
def get_subjects():
    global materias
    if request.method == "GET":
        return jsonify({"materias": materias})


@app.route('/horarios/<cod>', methods=['GET'])
def get_materia_horarios(cod):
    global horarios
    data = []
    if request.method == "GET":
        for horario in horarios:
            if horario["materia_cod"] == cod:
                data.append(horario)
        return jsonify({"horarios": data})


@app.route('/horarios', methods=['GET'])
def get_horarios():
    global horarios
    return jsonify({"horarios": horarios})


@app.route('/inscribir/horario', methods=["POST", "PUT"])
def select_horario():
    global horarios
    data = request.get_json(force=True)
    id = int(data.get("id", 0))
    if request.method == "POST":
        for horario in horarios:
            if horario["id"] == id:
                horario["is_selected"] = True
                return jsonify({"horario": horario})
        return jsonify({})

    # Unselect horario
    if request.method == "PUT":
        for horario in horarios:
            if horario["id"] == id:
                horario["is_selected"] = False
                return jsonify({"horario": horario})
        return jsonify({})


if __name__ == '__main__':
    app.run()
