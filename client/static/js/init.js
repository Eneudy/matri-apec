(function($){
  $(function(){

    $('.button-collapse').sideNav();

    // Dropdown menu
    $(".dropdown-button").dropdown();

    // Dropdown mobile menu
    $(".dropdown-button-mobile").dropdown();

    // Modal Activator 
    $(".modal").modal();


/*
    // Time Picker
    $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,   // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: true, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Limpiar', // text for clear-button
        canceltext: 'Cancelar', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
    });

    // Date Picker
    $('.datepicker').pickadate({
        //format: 'mm/dd/yyyy',
        format: 'dd/mm/yyyy',
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Ok',
        closeOnSelect: false, // Close upon selecting a date,
        firstDay: true // Monday first calendar day
      });

    // Modal Activator 
    $(".modal").modal();

    // Delete button confirmation
    $(".delete-link").on("click", null, function(){
        return confirm("Desea continuar?");
    });

    $(".myReadonly a").click(function(){
    	alert("** No disponible **");
    	return false;
    });
*/

  }); // end of document ready
})(jQuery); // end of jQuery name space