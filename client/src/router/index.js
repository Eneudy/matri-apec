import Vue from 'vue';
import Router from 'vue-router';
import Ping from '@/components/Ping';
import HelloWorld from '@/components/HelloWorld';
import NotFound from '@/components/NotFound';

import Inicio from '@/views/Inicio';
import Academico from '@/views/Academico';
import Caja from '@/views/Caja';
import Inscripcion from '@/views/Inscripcion';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'inicio',
      component: Inicio,
    },
    {
      path: '/academico',
      name: 'academico',
      component: Academico,
    },
    {
      path: '/inscripcion',
      name: 'inscripcion',
      component: Inscripcion,
    },
    {
      path: '/caja',
      name: 'caja',
      component: Caja,
    },
    {
      path: '/ping',
      name: 'ping',
      component: Ping,
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound,
    },
  ],
});
